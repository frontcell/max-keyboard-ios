//
//  ViewController.swift
//  max-ios-keyboard
//
//  Created by Oscar Kockum on 2018-03-13.
//  Copyright © 2018 Oscar Kockum. All rights reserved.
//

import UIKit

class StepThreeVC: UIViewController {

    @IBOutlet weak var thumbsStackView: UIStackView!
    @IBOutlet var burgerInfoCollection: [UILabel]!
    @IBOutlet var burgerTitleCollection: [UILabel]!
    @IBOutlet weak var readMoreButton: UIButton!
    
    @IBOutlet weak var leadingSpace: NSLayoutConstraint!
    @IBOutlet weak var topSpace: NSLayoutConstraint!
    @IBOutlet var burgerInfoTextStackViewCollection: [UIStackView]!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        thumbsStackView.spacing = CGFloat(DeviceMargins.thumbsSpacing)
        burgerInfoCollection.forEach { (label) in
            label.font = UIFont(name: label.font.fontName, size: DeviceMargins.burgerListSize)
        }
        burgerTitleCollection.forEach { (label) in
            label.font = UIFont(name: label.font.fontName, size: DeviceMargins.burgerListTitleSize)
        }
        burgerInfoTextStackViewCollection.forEach { (stackview) in
            stackview.spacing = DeviceMargins.burgerThumbVerticalSpacing
        }
        leadingSpace.constant = DeviceMargins.burgerListLeading
        
        topSpace.constant = DeviceMargins.burgerPageTop
        
        let link = NSLocalizedString("read_more_link", comment: "")
        let readMore = NSMutableAttributedString(string: "\(NSLocalizedString("read_more", comment: ""))\(link)")
        let range = (readMore.string as NSString).range(of: link)
        readMore.addAttributes([.foregroundColor : UIColor(red: 0, green: 200 / 255.0, blue: 125.0 / 255.0, alpha: 1),
                                .underlineStyle : NSUnderlineStyle.styleSingle.rawValue], range: range)
        readMoreButton.setAttributedTitle(readMore, for: .normal)
        
    }
    
    @IBAction func readMore(_ sender: Any) {
        let url = "http://www.\(NSLocalizedString("read_more_link", comment: ""))"
        UIApplication.shared.open(URL(string: url)!, options: [:], completionHandler: nil)
    }
    
}

