//
//  ViewController.swift
//  max-ios-keyboard
//
//  Created by Oscar Kockum on 2018-03-13.
//  Copyright © 2018 Oscar Kockum. All rights reserved.
//

import UIKit

class StepOneVC: UIViewController {

    @IBOutlet weak var movingView: MovingBackgroundView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(self.onAppResumed),
            name: NSNotification.Name.UIApplicationDidBecomeActive,
            object: nil)
    }

    @objc func onAppResumed() {
       movingView.isAnimating = true
    }
    
    override func viewDidAppear(_ animated: Bool) {
        movingView.isAnimating = true
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        movingView.isAnimating = false
    }
  
    

}

