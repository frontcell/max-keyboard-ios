//
//  DeviceMargins.swift
//  max-ios-keyboard
//
//  Created by Oscar Kockum on 2018-04-24.
//  Copyright © 2018 Oscar Kockum. All rights reserved.
//

import Foundation

class DeviceMargins {
    class var stepsSpacing : Int {
        if Device.IS_IPHONE_X  {
            return 40
        } else if Device.IS_IPHONE_6 {
            return 25
        } else if Device.IS_IPHONE_6P {
            return 30
        } else {
            return 10
        }
    }
    
    class var thumbsSpacing : Int {
        if Device.IS_IPHONE_X  {
            return 10
        } else if Device.IS_IPHONE_6P {
            return 10
        } else if Device.IS_IPHONE_6 {
            return 0
        } else {
            return 0
        }
    }
    
    class var stepsTop : CGFloat {
        if Device.IS_IPHONE_X  {
            return 50
        } else if Device.IS_IPHONE_6P {
            return 50
        } else if Device.IS_IPHONE_6 {
            return 35
        } else {
            return 25
        }
    }
    
    class var stepsBottom : CGFloat {
        if Device.IS_IPHONE_X  {
            return 65
        } else if Device.IS_IPHONE_6P {
            return 65
        } else if Device.IS_IPHONE_6 {
            return 65
        } else {
            return 35
        }
    }
    
    class var burgerListSize : CGFloat {
        if Device.IS_IPHONE_X  {
            return 13
        } else if Device.IS_IPHONE_6P {
            return 13
        } else if Device.IS_IPHONE_6 {
            return 12
        } else {
            return 12
        }
    }
    
    class var burgerListTitleSize : CGFloat {
        if Device.IS_IPHONE_X  {
            return 12
        } else if Device.IS_IPHONE_6P {
            return 12
        } else if Device.IS_IPHONE_6 {
            return 11
        } else {
            return 11
        }
    }
    
    class var burgerListLeading : CGFloat {
        if Device.IS_IPHONE_X  {
            return 56
        } else if Device.IS_IPHONE_6P {
            return 56
        } else if Device.IS_IPHONE_6 {
            return 50
        } else {
            return 30
        }
    }
    
    class var burgerPageTop : CGFloat {
        if Device.IS_IPHONE_X  {
            return 40
        } else if Device.IS_IPHONE_6P {
            return 40
        } else if Device.IS_IPHONE_6 {
            return 30
        } else {
            return 20
        }
    }
    
    class var burgerThumbVerticalSpacing : CGFloat {
        if Device.IS_IPHONE_X  {
            return 2
        } else if Device.IS_IPHONE_6P {
            return 2
        } else if Device.IS_IPHONE_6 {
            return 2
        } else {
            return 0
        }
    }
    
    
    
}


class Device {
    // iDevice detection code
    static let IS_IPAD             = UIDevice.current.userInterfaceIdiom == .pad
    static let IS_IPHONE           = UIDevice.current.userInterfaceIdiom == .phone
    static let IS_RETINA           = UIScreen.main.scale >= 2.0
    
    static let SCREEN_WIDTH        = Int(UIScreen.main.bounds.size.width)
    static let SCREEN_HEIGHT       = Int(UIScreen.main.bounds.size.height)
    static let SCREEN_MAX_LENGTH   = Int( max(SCREEN_WIDTH, SCREEN_HEIGHT) )
    static let SCREEN_MIN_LENGTH   = Int( min(SCREEN_WIDTH, SCREEN_HEIGHT) )
    
    static let IS_IPHONE_4_OR_LESS = IS_IPHONE && SCREEN_MAX_LENGTH  < 568
    static let IS_IPHONE_5         = IS_IPHONE && SCREEN_MAX_LENGTH == 568
    static let IS_IPHONE_6         = IS_IPHONE && SCREEN_MAX_LENGTH == 667
    static let IS_IPHONE_6P        = IS_IPHONE && SCREEN_MAX_LENGTH == 736
    static let IS_IPHONE_X         = IS_IPHONE && SCREEN_MAX_LENGTH == 812
}
