//
//  MovingBackgroundView.swift
//  max-ios-keyboard
//
//  Created by Oscar Kockum on 2018-04-20.
//  Copyright © 2018 Oscar Kockum. All rights reserved.
//

import UIKit

class MovingBackgroundView: UIView {

    var leftFloatingImageView = UIImageView(image: #imageLiteral(resourceName: "floating_bg"))
    var rightFloatingImageView = UIImageView(image: #imageLiteral(resourceName: "floating_bg"))
    var topImageView = UIImageView(image: #imageLiteral(resourceName: "greenburgeremoji"))
    var combinedView = UIView()
    var imageSize : CGSize!
    
    
    fileprivate func initViews() {
        imageSize = #imageLiteral(resourceName: "floating_bg").size
        translatesAutoresizingMaskIntoConstraints = false
        combinedView.translatesAutoresizingMaskIntoConstraints = false
        leftFloatingImageView.contentMode = .center
        rightFloatingImageView.contentMode = .center
        addSubview(combinedView)
        combinedView.addSubview(leftFloatingImageView)
        combinedView.addSubview(rightFloatingImageView)
        addSubview(topImageView)
        topImageView.contentMode = .scaleAspectFit
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initViews()
    }
    
    override func layoutSubviews() {
        combinedView.frame =  CGRect(x: 0, y: 0, width: imageSize.width * 2, height: frame.size.height)
        topImageView.frame =  CGRect(x: 0, y: 0, width: frame.size.width, height: frame.size.height)
        leftFloatingImageView.frame = CGRect(x: 0, y: 0, width: imageSize.width, height: frame.size.height)
        rightFloatingImageView.frame = CGRect(x: imageSize.width, y: 0, width: imageSize.width, height: frame.size.height)
        
    }
    
    var isAnimating : Bool = false {
        didSet {
            if isAnimating {
                combinedView.layer.add(setupAnimation(from: imageSize.width, to: 0), forKey: "move")
            } else {
                combinedView.layer.removeAllAnimations()
            }
        }
    }
    
    fileprivate func setupAnimation(from : CGFloat, to : CGFloat) -> CAAnimation {
        let animation = CAKeyframeAnimation()
        animation.keyPath = "position"
        animation.duration = 13
        animation.repeatCount = Float.greatestFiniteMagnitude
        //animation.isAdditive = true // Make the animation position values that we later generate relative values.
        
        var values = [NSValue]()
        for (idx, x) in (Int(to)..<Int(from)).reversed().enumerated() {
            let xPos = CGFloat(x)
            //let point = CGPoint(x: xPos, y: yPos * 10)  // The 10 is to give it some amplitude.
            let point = CGPoint(x: xPos, y: frame.size.height / 2 + 15*sin(CGFloat(4.0 * .pi)/from*CGFloat(idx)))  // The 10 is to give it some amplitude.
            values.append(NSValue(cgPoint: point))
        }
        animation.values = values
        return animation
    }
    
    
}
