//
//  ViewController.swift
//  max-ios-keyboard
//
//  Created by Oscar Kockum on 2018-03-13.
//  Copyright © 2018 Oscar Kockum. All rights reserved.
//

import UIKit

class StepTwoVC: UIViewController {

    @IBOutlet weak var topStackView: UIStackView!
    @IBOutlet weak var topCenterConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var stepStackView: UIStackView!
    @IBOutlet weak var stepTop: NSLayoutConstraint!
    @IBOutlet weak var stepBottom: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        stepStackView.arrangedSubviews.forEach { (view) in
            view.alpha = 0
        }
        stepStackView.spacing = CGFloat(DeviceMargins.stepsSpacing)
        stepTop.constant = DeviceMargins.stepsTop
        stepBottom.constant = DeviceMargins.stepsBottom
        topCenterConstraint.constant = -400
        view.clipsToBounds = true
    }
    
    func delayWithSeconds(_ seconds: Double, completion: @escaping () -> ()) {
        DispatchQueue.main.asyncAfter(deadline: .now() + seconds) {
            completion()
        }
    }
    
    private func animateAlpha(views : [UIView], index : Int) {
        UIView.animate(withDuration: 0.2, animations: {
            views[index].alpha = 1
        }) { (finished) in
            if index < (views.count - 1) {
                self.animateAlpha(views: views, index: index + 1)
            }
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        UIView.animate(withDuration: 0.2, animations: {
            self.topCenterConstraint.constant = 0
            self.view.layoutIfNeeded()
        }) { (finished) in
            self.animateAlpha(views: self.stepStackView.arrangedSubviews, index: 0)
        }
    }
    
    
    @IBAction func actionSettings(_ sender: Any) {
        UIApplication.shared.open(URL(string:UIApplicationOpenSettingsURLString)!, options:[:], completionHandler: nil)
    }

}

