//
//  UIKitExtensions.swift
//  max-ios-keyboard
//
//  Created by Oscar Kockum on 2018-04-24.
//  Copyright © 2018 Oscar Kockum. All rights reserved.
//

import Foundation

extension UIView {

    @IBInspectable var cornerRadius: CGFloat {
        get {
            return layer.cornerRadius
        }
        set {
            layer.cornerRadius = newValue
        }
    }
}

