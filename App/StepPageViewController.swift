//
//  StepPageViewController.swift
//  max-ios-keyboard
//
//  Created by Oscar Kockum on 2018-04-20.
//  Copyright © 2018 Oscar Kockum. All rights reserved.
//

import UIKit

class StepPageViewController: UIViewController, UIPageViewControllerDelegate, UIPageViewControllerDataSource {
   
    fileprivate lazy var pages: [UIViewController] = {
        return [
            storyboard!.instantiateViewController(withIdentifier: "StepOne"),
            storyboard!.instantiateViewController(withIdentifier: "StepTwo"),
            storyboard!.instantiateViewController(withIdentifier: "StepThree"),
        ]
    }()

    @IBOutlet weak var pageContainerView: UIView!
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var termsText: UILabel!
    @IBOutlet weak var rightArrow: UIImageView!
    @IBOutlet weak var rightArrowWidth: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let pageViewController = UIPageViewController(transitionStyle: .scroll, navigationOrientation: .horizontal, options: nil)
        pageViewController.dataSource = self
        pageViewController.delegate = self
        addChildViewController(pageViewController)
        pageContainerView.addSubview(pageViewController.view)
        pageViewController.view.translatesAutoresizingMaskIntoConstraints = false
        pageViewController.view.topAnchor.constraint(equalTo: pageContainerView.topAnchor).isActive = true
        pageViewController.view.leadingAnchor.constraint(equalTo: pageContainerView.leadingAnchor).isActive = true
        pageViewController.view.trailingAnchor.constraint(equalTo: pageContainerView.trailingAnchor).isActive = true
        pageViewController.view.bottomAnchor.constraint(equalTo: pageContainerView.bottomAnchor).isActive = true
        pageViewController.didMove(toParentViewController: self)
        
        pageViewController.setViewControllers([pages.first!], direction: .forward, animated: false, completion: nil)
        
        
    }
    
    fileprivate func animateArrow() {
        self.rightArrowWidth.constant = 10
        view.layoutIfNeeded()
        UIView.animate(withDuration:0.5) {
            self.rightArrowWidth.constant = 40
            self.view.layoutIfNeeded()
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        //animateArrow()
    }

    @IBAction func actionTerms(_ sender: Any) {
        UIApplication.shared.open(URL(string: "https://www.max.se/sv/Om-webbplatsen/Villkor--cookies/")!, options: [:], completionHandler: nil)
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        guard let viewControllerIndex = pages.index(of: viewController),
             viewControllerIndex + 1 < pages.count else {
            return nil
        }
        return pages[viewControllerIndex + 1]
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        guard let viewControllerIndex = pages.index(of: viewController),
            viewControllerIndex - 1 >= 0 else {
                return nil
        }
        return pages[viewControllerIndex - 1]
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        guard completed, let viewController = pageViewController.viewControllers?.last, let index = pages.index(of: viewController) else {
            return
        }
        pageControl.currentPage = index
        rightArrow.isHidden = true
        //if index == 0 {
        //    animateArrow()
        //}
        
        
    }

}
