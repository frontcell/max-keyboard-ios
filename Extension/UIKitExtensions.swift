//
//  UIKitExtensions.swift
//  keyboard-extension
//
//  Created by Oscar Kockum on 2018-03-14.
//  Copyright © 2018 Oscar Kockum. All rights reserved.
//

import UIKit

extension UIView {
    func pinToMargins(view : UIView, padding : CGFloat = 0) {
        translatesAutoresizingMaskIntoConstraints = false
        topAnchor.constraint(equalTo: view.topAnchor, constant:padding).isActive = true
        leftAnchor.constraint(equalTo: view.leftAnchor, constant:padding).isActive = true
        rightAnchor.constraint(equalTo: view.rightAnchor, constant:-padding).isActive = true
        bottomAnchor.constraint(equalTo: view.bottomAnchor, constant:-padding).isActive = true
    }
}

extension UILabel {
    func addTextAnimation(_ duration:CFTimeInterval) {
        guard layer.animation(forKey: "TextAnimation") == nil else {
            return
        }
        let animation = CATransition()
        animation.timingFunction = CAMediaTimingFunction(name:
            kCAMediaTimingFunctionEaseInEaseOut)
        animation.type = kCATransitionFade
        animation.duration = duration
        layer.add(animation, forKey: "TextAnimation")
    }
    
    func addPulseAnimation() {
        guard layer.animation(forKey: "PulseAnimation") == nil else {
            return
        }
        let pulseAnimation:CABasicAnimation = CABasicAnimation(keyPath: "transform.scale")
        pulseAnimation.duration = 1.0
        pulseAnimation.fromValue = NSNumber(value: 0)
        pulseAnimation.toValue = NSNumber(value: 1.0)
        pulseAnimation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        pulseAnimation.autoreverses = false
        pulseAnimation.repeatCount = 1
        layer.add(pulseAnimation, forKey: "PulseAnimation")
    }
    
}

extension UIImage {
    
    func resizeWith(percentage: CGFloat) -> UIImage? {
        let imageView = UIImageView(frame: CGRect(origin: .zero, size: CGSize(width: size.width * percentage, height: size.height * percentage)))
        imageView.contentMode = .scaleAspectFit
        imageView.image = self
        UIGraphicsBeginImageContextWithOptions(imageView.bounds.size, false, scale)
        guard let context = UIGraphicsGetCurrentContext() else { return nil }
        imageView.layer.render(in: context)
        guard let result = UIGraphicsGetImageFromCurrentImageContext() else { return nil }
        UIGraphicsEndImageContext()
        return result
    }

}
