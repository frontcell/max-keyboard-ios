//
//  KeyboardViewController.swift
//  keyboard-extension
//
//  Created by Oscar Kockum on 2018-03-13.
//  Copyright © 2018 Oscar Kockum. All rights reserved.
//

import UIKit
import KeyboardLayoutEngine

class KeyboardViewController: UIInputViewController {

    @IBOutlet var nextKeyboardButton: UIButton!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var bottomBar: UIView!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var eraseButton: UIButton!
    @IBOutlet weak var characterKeyboardView: UIView!
    @IBOutlet weak var abcButton: UIButton!
    @IBOutlet weak var noFullAccessView: UIView!
    @IBOutlet weak var noFullAccessLabel: UILabel!
    
    var heightAnchor : NSLayoutConstraint?
    
    fileprivate lazy var images: [(UIImage,UIImage)] = {
        return [
            (#imageLiteral(resourceName: "bbq_padding_small"),#imageLiteral(resourceName: "bbq_padding")),
            (#imageLiteral(resourceName: "chillicheese_padding_small"),#imageLiteral(resourceName: "chillicheese_padding")),
            (#imageLiteral(resourceName: "crispy_padding_small"),#imageLiteral(resourceName: "crispy_padding")),
            (#imageLiteral(resourceName: "halloumi_padding_small"),#imageLiteral(resourceName: "halloumi_padding")),
            (#imageLiteral(resourceName: "nuggets_padding_small"),#imageLiteral(resourceName: "nuggets_padding")),
            (#imageLiteral(resourceName: "salad_padding_small"),#imageLiteral(resourceName: "salad_padding")),
            (#imageLiteral(resourceName: "umami_padding_small"),#imageLiteral(resourceName: "umami_padding"))
        ]
    }()
    
    
    
    lazy var tracker : GAI? = {
        guard let gai = GAI.sharedInstance() else {
            assert(false, "Google Analytics not configured correctly")
            return nil
        }
        gai.tracker(withTrackingId: "UA-47077582-3")
        return gai
    }()
    
    func neededHeight(width : CGFloat) -> CGFloat {
        guard let layout = collectionView.collectionViewLayout as? HorizontalGridLayout else {
            return 0
        }
        let height = layout.heightNeeded(width: width)
        print("height: \(height), width: \(width)")
        return height
    }
    
    
    var keyboardWidth = CGFloat(0) {
        didSet {
            if heightAnchor == nil {
                heightAnchor = collectionView.heightAnchor.constraint(equalToConstant: neededHeight(width: keyboardWidth))
                heightAnchor?.priority = UILayoutPriority(rawValue: 999.0)
                heightAnchor?.isActive = true
            } else {
                heightAnchor?.constant = neededHeight(width: keyboardWidth)
            }
        }
    }
    
    override func updateViewConstraints() {
        super.updateViewConstraints()
        keyboardWidth = view.frame.width
    }
    
    var currentKeyboard : KeyboardLayout?
    var customKeyboard = CustomKeyboard()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //let event = GAIDictionaryBuilder.createEvent(withCategory: "view", action: "load", label: "", value:nil)
        //tracker.defaultTracker.send(event!.build() as! [AnyHashable : Any]!)

        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.register(EmojiCell.self, forCellWithReuseIdentifier: EmojiCell.identifier)
        
        characterKeyboardView.addSubview(customKeyboard)
        customKeyboard.delegate = self
        customKeyboard.translatesAutoresizingMaskIntoConstraints = false
        customKeyboard.topAnchor.constraint(equalTo: characterKeyboardView.topAnchor, constant:40).isActive = true
        customKeyboard.bottomAnchor.constraint(equalTo: characterKeyboardView.bottomAnchor).isActive = true
        customKeyboard.leftAnchor.constraint(equalTo: characterKeyboardView.leftAnchor).isActive = true
        customKeyboard.rightAnchor.constraint(equalTo: characterKeyboardView.rightAnchor).isActive = true
       
        if Device.IS_IPHONE_X {
            nextKeyboardButton.isHidden = true
        } else {
            nextKeyboardButton.addTarget(self, action: #selector(handleInputModeList(from:with:)), for: .allTouchEvents)
        }
        
        /*if Device.IS_IPHONE_X {
         bottomBar.isHidden = true
           eraseButton.isHidden = false
        } else {
            eraseButton.isHidden = true
         
        }*/
    }

    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        currentKeyboard?.setNeedsLayout()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        updateKeyboardAccessStatusLabel()
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        keyboardWidth = KeyboardSizeConverter.correctWidth(size: size)
        
    }
    
    @IBAction func actionErase(_ sender: Any) {
        let proxy = self.textDocumentProxy
        proxy.deleteBackward()
    }
    
    @IBAction func actionSwitchKeyboard(_ sender: Any) {
        //handleInputModeList(from: sender, with: sender.)
    }
    
    @IBAction func showABCKeyboard(_ sender: Any) {
        characterKeyboardView.isHidden = false
    }
    
    fileprivate func notifyImageCopied() {

        statusLabel.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
        statusLabel.text = NSLocalizedString("copied_to_clipboard", comment: "")
        statusLabel.textColor = UIColor.gray
        UIView.animate(withDuration: 1.0,
                       delay: 0,
                       usingSpringWithDamping: 0.6,
                       initialSpringVelocity: 6.0,
                       options: .allowUserInteraction,
                       animations: {
                        self.statusLabel.transform = .identity
                        },
                       completion: nil)
        
    }
    
    fileprivate func hasFullAccess() -> Bool {
        var access = false
        if #available(iOSApplicationExtension 11.0, *) {
            access = hasFullAccess
        } else {
            if !UIPasteboard.general.hasStrings {
                UIPasteboard.general.string = "test"
                access = UIPasteboard.general.hasStrings
                UIPasteboard.general.string = "" // reset
            } else {
                access = true
            }
        }
        return access
    }
    
    fileprivate func updateKeyboardAccessStatusLabel() {
        noFullAccessView.isHidden = hasFullAccess()
        noFullAccessLabel.text = NSLocalizedString("no_access", comment: "")
    }
   
    
}

extension KeyboardViewController : UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return images.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: EmojiCell.identifier, for: indexPath) as! EmojiCell
        cell.emoji = images[indexPath.row].1
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if hasFullAccess() {
            let image = images[indexPath.row].0
            UIPasteboard.general.image = image
            notifyImageCopied()
        }
    }

}

extension KeyboardViewController : CustomKeyboardDelegate {
    // MARK: CustomKeyboardDelegate
    func customKeyboard(_ customKeyboard: CustomKeyboard, keyboardButtonPressed keyboardButton: KeyboardButton) {
        if customKeyboard == self.customKeyboard {
            if keyboardButton.identifier == "customButton" {
                print("custom button pressed")
            }
        }
    }
    
    func customKeyboard(_ customKeyboard: CustomKeyboard, keyButtonPressed key: String) {
        if customKeyboard == self.customKeyboard {
            textDocumentProxy.insertText(key)
        }
    }
    
    func customKeyboardGlobeButtonPressed(_ customKeyboard: CustomKeyboard) {
        characterKeyboardView.isHidden = true
    }
    
    func customKeyboardSpaceButtonPressed(_ customKeyboard: CustomKeyboard) {
        if customKeyboard == self.customKeyboard {
            textDocumentProxy.insertText(" ")
        }
    }
    
    func customKeyboardBackspaceButtonPressed(_ customKeyboard: CustomKeyboard) {
        if customKeyboard == self.customKeyboard {
            textDocumentProxy.deleteBackward()
        }
    }
    
    func customKeyboardReturnButtonPressed(_ customKeyboard: CustomKeyboard) {
        if customKeyboard == self.customKeyboard {
            textDocumentProxy.insertText("\n")
        }
    }
}

class EmojiCell : UICollectionViewCell {
    static let identifier = "EmojiCell"
    static let imageTag = 123
    
    
    var emoji : UIImage? {
        didSet {
            var imageView = contentView.viewWithTag(EmojiCell.imageTag) as? UIImageView
            if imageView == nil {
                let iw = UIImageView(frame: CGRect.zero)
                iw.contentMode = .scaleAspectFit
                iw.tag = EmojiCell.imageTag
                imageView = iw
                iw.translatesAutoresizingMaskIntoConstraints = false
                contentView.addSubview(iw)
                iw.pinToMargins(view: contentView, padding: 5)
            }
            imageView?.image = emoji
        }
    }
}




