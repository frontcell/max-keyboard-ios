//
//  HorizontalGridLayout.swift
//  keyboard-extension
//
//  Created by Oscar Kockum on 2018-03-14.
//  Copyright © 2018 Oscar Kockum. All rights reserved.
//

import UIKit

class KeyboardCollectionView : UICollectionView {
    
    let gridLayout = HorizontalGridLayout()
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        collectionViewLayout = gridLayout
    }
  
}


class HorizontalGridLayout: UICollectionViewLayout {
    
    var linesInLandscape = 1
    var linesInPortrait = 2

    var lines : Int {
        if UIScreen.main.bounds.width > UIScreen.main.bounds.height {
            return linesInLandscape
        } else {
            return linesInPortrait
        }
    }
    
    
    private var boundsSize = CGSize.zero
    private var itemLength = CGFloat(0)
    private var contentWidth = CGFloat(0)
    private var columns = CGFloat(0)

    
    override func prepare() {
        updateLayout()
    }
    
    func updateLayout(width : CGFloat? = nil) {
        guard let cv = collectionView else {
            return
        }
        
        let w = width ?? cv.frame.width
        
        let items = cv.numberOfItems(inSection: 0)
        columns = ceil(CGFloat(items) / CGFloat(lines))
        itemLength = w / columns
        contentWidth = itemLength * columns
        boundsSize = self.collectionView!.bounds.size
    }
    
    func heightNeeded(width : CGFloat) -> CGFloat {
        updateLayout(width: width)
        return max(itemLength * CGFloat(lines),150)
    }
    
    override var collectionViewContentSize: CGSize {
        return CGSize(width: contentWidth, height: boundsSize.height)
    }
    
    override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
        guard let cv = collectionView else {
            return nil
        }
        var allAttributes = [UICollectionViewLayoutAttributes]()
        for i in 0..<cv.numberOfItems(inSection: 0) {
            let attr = self.computeLayoutAttributesForCellAtIndexPath(indexPath: IndexPath(row: i, section: 0))
            allAttributes.append(attr)
        }
        return allAttributes
    }
    
    override func layoutAttributesForItem(at indexPath: IndexPath) -> UICollectionViewLayoutAttributes? {
        return self.computeLayoutAttributesForCellAtIndexPath(indexPath: indexPath)
    }
    
    override func shouldInvalidateLayout(forBoundsChange newBounds: CGRect) -> Bool {
        return true
    }
    
    func computeLayoutAttributesForCellAtIndexPath(indexPath: IndexPath) -> UICollectionViewLayoutAttributes {
        
        let attr = UICollectionViewLayoutAttributes(forCellWith: indexPath)
        attr.size = CGSize(width: itemLength, height: itemLength)
        
        let column = indexPath.row / lines
        let line = indexPath.row % lines
        
        let frame = CGRect(x: CGFloat(column) * itemLength, y: CGFloat(line) * itemLength, width: itemLength, height: itemLength)
        attr.frame = frame
        
        return attr
    }
}
