//
//  KeyboardSizeConverter.swift
//  keyboard-extension
//
//  Created by Oscar Kockum on 2018-04-21.
//  Copyright © 2018 Oscar Kockum. All rights reserved.
//

import Foundation

class KeyboardSizeConverter {
    class func correctWidth(size : CGSize) -> CGFloat {
        if Device.IS_IPHONE_X {
            return size.width > 600 ? 812 : 375
        } else {
            return size.width
        }
    }

}

